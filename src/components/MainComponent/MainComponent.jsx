import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import CardModal from './CardModal';
import styled from 'styled-components';
import Header from '../Header/Header';

const MainComponent = () => {
  const [products, setProducts] = useState([]);
  const [cartCount, setCartCount] = useState(parseInt(localStorage.getItem('cartCount')) || 0);
  const [favoritesCount, setFavoritesCount] = useState(parseInt(localStorage.getItem('favoritesCount')) || 0);
  const [showModal, setShowModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('/products.json');
        const data = await response.json();
        setProducts(data);
      } catch (error) {
        console.error('Error fetching products:', error);
      }
    };
    fetchData();
  }, []);

  const handleAddToCart = () => {
    const setCartCounter = cartCount + 1;
    setCartCount(setCartCounter);
    setShowModal(false);
    localStorage.setItem('cartCount', JSON.stringify(setCartCounter));
  };

  const handleToggleFavorites = (id) => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    if (favorites.includes(id)) {
      const updatedFavorites = favorites.filter(item => item !== id);
      setFavoritesCount(favoritesCount - 1);
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
      localStorage.setItem('favoritesCount', favoritesCount - 1);
    } else {
      const updatedFavorites = [...favorites, id];
      setFavoritesCount(favoritesCount + 1);
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
      localStorage.setItem('favoritesCount', favoritesCount + 1);
    }
  };

  return (
  <>
  <Header cardCount={cartCount} favoritesCount={favoritesCount}/>
  <MainContainer>
      {products.map((product, index) => (
        <ProductCard
          key={index}
          product={product}
          addToCart={() => { setShowModal(true); setSelectedProduct(product); }}
          addToFavorites={handleToggleFavorites}
        />
      ))}
      <CardModal
        isOpen={showModal}
        handleClose={() => setShowModal(false)}
        handleAddToCart={handleAddToCart}
        productName={selectedProduct ? selectedProduct.name : ''}
      />
    </MainContainer>
  </>  
  );
};

const MainContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  grid-gap: 20px;
  padding: 20px;
  position: relative;
  top: 50px; 
`;

export default MainComponent;