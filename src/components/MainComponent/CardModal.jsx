import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const CardModal = ({ isOpen, handleClose, handleAddToCart, productName }) => {
  return (
    isOpen && (
      <ModalOverlay onClick={handleClose}>
        <ModalContent onClick={(e) => e.stopPropagation()}>
          <CloseButton onClick={handleClose}>X</CloseButton>
          <Title>Підтвердження додавання товару в кошик</Title>
          <p>Ви впевнені, що хочете додати товар "{productName}" в кошик?</p>

          <ButtonContainer>
            <AddToCartButton onClick={handleAddToCart}>Додати до кошика</AddToCartButton>
          </ButtonContainer>
        </ModalContent>
      </ModalOverlay>
    )
  );
};

CardModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleAddToCart: PropTypes.func.isRequired,
  productName: PropTypes.string.isRequired,
};

const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1;
`;

const ModalContent = styled.div`
  background-color: #fff;
  padding: 20px;
  border-radius: 5px;
  text-align: center;
  position: relative;
`;

const CloseButton = styled.span`
  position: absolute;
  top: 10px;
  right: 10px;
  cursor: pointer;
`;

const ButtonContainer = styled.div`
  margin-top: 20px;
`;

const Title = styled.h2`
  margin-bottom: 20px;
`;

const AddToCartButton = styled.button`
  background-color: #007bff;
  color: #fff;
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  transition: background-color 0.3s ease;
  &:hover {
    background-color: #0056b3;
  }
`;

export default CardModal;