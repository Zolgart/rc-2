import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FaStar } from 'react-icons/fa';

const ProductCard = ({ product, addToCart, addToFavorites }) => {
  const { id, name, price, image, article, color } = product;

  const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
  const [isFavorite, setIsFavorite] = useState( favorites.includes(id));

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setIsFavorite(favorites.includes(id));
  }, [id]);
  
  const handleToggleFavorites = () => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    if (favorites.includes(id)) {
      setIsFavorite(true);
    } else {
      setIsFavorite(false);
    }
  };

  return (
    <CardContainer>
      <ProductImage src={image} alt={name} />
      <h3>{name}</h3>
      <p>Ціна: ${price}</p>
      <p>Артикул: {article}</p>
      <p>Колір: {color}</p>
      <ButtonContainer>
        <AddToCartButton onClick={addToCart}>Додати до кошика</AddToCartButton>
        <FavoritesButton onClick={() => {addToFavorites(id); handleToggleFavorites()}} isFavorite={isFavorite}>
          <FaStar />
        </FavoritesButton>
      </ButtonContainer>
    </CardContainer>
  );
};

ProductCard.propTypes = {
  product: PropTypes.object.isRequired,
  addToCart: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired
};

const CardContainer = styled.div`
  border: 1px solid #ccc;
  border-radius: 5px;
  padding: 10px;
  margin-bottom: 20px;
  background-color: #fff;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

const ProductImage = styled.img`
  width: 100%;
  max-height: 200px;
  object-fit: cover;
  border-radius: 5px;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 10px;
`;

const AddToCartButton = styled.button`
  background-color: #007bff;
  color: #fff;
  padding: 5px 10px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  transition: background-color 0.3s ease;
  &:hover {
    background-color: #0056b3;
  }
`;

const FavoritesButton = styled.button`
  background-color: transparent;
  border: none;
  cursor: pointer;
  font-size: 1.5rem;
  color: ${({ isFavorite }) => (isFavorite ? 'gold' : 'gray')};
  transition: color 0.3s ease;
`;

export default ProductCard;